import appdaemon.plugins.hass.hassapi as hass

class Global(hass.Hass):

    def initialize(self):
        self.color_temp = 6500 if self.is_sun_up() else 2200
   

    def is_sun_up(self):
        if self.get_state("sun.sun") == "above_horizon":
            return True
        else:
            return False
