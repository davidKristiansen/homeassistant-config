import appdaemon.plugins.hass.hassapi as hass

class MotionLights(hass.Hass):

    def initialize(self):
        self.trigger = self.args["trigger"]
        self.triggee = self.args["triggee"]
        self.timeout_s = self.args["timeout_s"]
        self.timeout_handler = None
   
        self.listen_state(self.state_changed, self.trigger)
        

    def state_changed(self, entity, attribute, old, new, kwargs):

        if new == "on":
            state = "on"
            if self.timeout_handler:
                self.cancel_timer(self.timeout_handler)
            self.turn_on(self.triggee)
        else:
            state = "off"
            self.timeout_handler = self.run_in(self.light_off, self.timeout_s)

        self.log("{} is {}".format(self.friendly_name(entity), state))

    def light_off(self, kwargs):
        self.turn_off(self.triggee)
        self.timeout_handler = None
