import appdaemon.plugins.hass.hassapi as hass

class DevicePresenceTrigger(hass.Hass):

    def initialize(self):
        trackers = self.get_trackers()
        for tracker in trackers:
            self.listen_state(self.state_changed, tracker)
        self.state_changed(None, None, None, None, None)


    def state_changed(self, entity, attribute, old, new, kwargs):
        if self.noone_home():
            self.log("Nobody home")
            for device in self.args["devices"]:
                if self.get_state(device) == "on":
                    self.log("Turning off {}".format(self.friendly_name(device)))
                    self.turn_off(device)
        elif self.anyone_home():
            self.log("Someone is home")
            for device in self.args["devices"]:
                if self.get_state(device) == "off":
                    self.log("Turning on {}".format(self.friendly_name(device)))
                    self.turn_on(device)
