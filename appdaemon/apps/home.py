import appdaemon.plugins.hass.hassapi as hass

class DevicePresenceTrigger(hass.Hass):

    def initialize(self):
        if not self.is_anybody_home():
            self.turn_off("light.all")
        else:
            self.turn_off("light.all")

    def is_anybody_home(self):
        for device in self.args["devices"]:
            if self.get_state(device) == "home":
                return True
        return False
   
